package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    
    protected Integer setVar(String varName, Integer varValue) {
    	globalSymbols.setSymbol(varName, varValue);
    	return varValue;
    }
    
    protected Integer getVar(String varName) {
    	return globalSymbols.getSymbol(varName);
    }
    
    protected void createVar(String varName) {
    	globalSymbols.newSymbol(varName);
    }
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer bitOR(Integer x, Integer y) {
		return x|y;
	}
	
	protected Integer bitAND(Integer x, Integer y) {
		return x&y;
	}
	
	protected Integer compare(Integer x, Integer y) {
		if(x==y) {
			return 1;
		}
		return 0;
	}
	
	protected Integer div(Integer x, Integer y) throws RuntimeException{
		if(y==0) {
			throw new RuntimeException("Don't divide by zero!");
		}
		return x/y;
	}
}
